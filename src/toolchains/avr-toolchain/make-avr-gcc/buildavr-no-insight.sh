#!/bin/bash

# AVR GNU development tools install script
# $Id: buildavr.sh,v 1.21 2004/11/09 02:25:48 rmoffitt Exp $
# Modified by AJ Erasmus    2005/03/22
# Use the same source and patches as for WinAVR 20050214
# Updated script to compile the C and C++ compilers, as well
# as allow the output of dwarf-2 debug information 2005/03/25
# Updated script to add new devices 2005/03/26


# Copyright (C) 2003-2004 Rod Moffitt rod@rod.info http://rod.info
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

# run the script as is to get a list of the files necessary to build,
# takes optionally three arguments, the location of the source files,
# where to install and build the tools and the path where to output a
# log file

# start of configuration, edit these defaults to suit your environment, or
# simply override them via the command line - you will need write
# permission to $prefix (make sure it's empty or non-existent!)

# location of source tarballs
archive=${PWD}

# GNU tools will be installed under this directory
prefix=/home/sean/local2

# build log file - see this if any errors occur
buildlog=/tmp/buildavr.log

# end of configuration

# what are we building for?
target=avr

# the files below shouldn't be changed unless a new release has been made
# and I (Rod) haven't updated this script!
insightver=6.1
insightbase=insight-${insightver}
insighttar=${insightbase}.tar.bz2
#sourcefiles="${insighttar} insight-6.1_reg_window.patch"

binutilsver=2.18
binutilsbase=binutils-${binutilsver}
binutilstar=${binutilsbase}.tar.bz2
#sourcefiles="${sourcefiles} ${binutilstar} binutils-2.1n5-no-lma-eeprom.patch binutils-2.15-avr-coff.patch binutils-2.15-avr-new-devs.patch"
sourcefiles="${sourcefiles} ${binutilstar} binutils-patch-aa.diff binutils-patch-coff-avr.diff binutils-patch-avr-size.diff binutils-patch-newdevices.diff binutils-patch-bug5215.diff binutils-patch-newsections.diff binutils-makeinfo.patch"


gccver=4.2.2  
gcccore=gcc-${gccver}
gcccoretar=${gcccore}.tar.bz2
gccbase=gcc-${gccver}
#sourcefiles="${sourcefiles} ${gcccoretar} gcc-binary-constants.patch  gcc-3_4_x-avr-new-devs2.patch gcc-avr-misspelled-sig-1.patch"
#sourcefiles="${sourcefiles} ${gcccoretar} gcc-patch-attribute_alias.diff gcc-patch-dwarf.diff gcc-patch-newdevices.diff  gcc-patch-zz-atmega256x.diff"
sourcefiles="${sourcefiles} ${gcccoretar}  gcc-patch-0b-constants.diff gcc-patch-bug11259.diff gcc-patch-bug25672.diff gcc-patch-bug30243.diff gcc-patch-constraint-desc.diff gcc-patch-doc-progmem.diff gcc-patch-dwarf.diff gcc-patch-libiberty-Makefile.in.diff  gcc-patch-newdevices.diff gcc-patch-os_main-os_task.diff gcc-patch-zero_extend.diff gcc-patch-zz-atmega256x.diff"

avrlibcver=1.6.1
avrlibcbase=avr-libc-${avrlibcver}
avrlibctar=${avrlibcbase}.tar.bz2
sourcefiles="${sourcefiles} ${avrlibctar}"

function buildandinstall()
{
   mkdir -p $prefix/source $prefix/build

   cd $prefix/source

   echo "($0) installing binutils source"
   tar xvjf $archive/${binutilstar}
   cerror "binutils source installation failed"

   cd ${binutilsbase}

   echo "($0) patching binutils source"
   patch -p0 < $archive/binutils-patch-aa.diff
   cerror "binutils patching failed"
#
   patch -p0 < $archive/binutils-patch-bug5215.diff
   cerror "binutils patching failed"
#
   patch -p0 < $archive/binutils-patch-coff-avr.diff
   cerror "binutils patching failed"
#
   patch -p0 < $archive/binutils-patch-newdevices.diff
   cerror "binutils patching failed"
#
   patch -p0 < $archive/binutils-patch-newsections.diff
   cerror "binutils patching failed"
#
   patch -p0 < $archive/binutils-patch-avr-size.diff
   cerror "binutils patching failed"
#
   patch -p0 < $archive/binutils-makeinfo.patch
   cerror "binutils patching failed"
#

   mkdir -p ../../build/${binutilsbase}
   cd ../../build/${binutilsbase}

   echo "($0) configuring binutils source"
   ../../source/${binutilsbase}/configure -v --target=${target} \
      --prefix=$prefix --with-gnu-ld --with-gnu-as --quiet --enable-install-libbfd --with-dwarf2
   cerror "binutils configuration failed"

   echo "($0) building binutils"
   make  all install clean
   cerror "binutils build failed"

   # path to the newly installed binutils is needed to build GCC
   PATH=$prefix/bin:$PATH

   cd $prefix/source

   echo "($0) installing GCC source"
   tar xvjf $archive/${gcccoretar}
   cerror "GCC source installation failed"

   cd ${gccbase}
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-0b-constants.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-bug11259.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-bug25672.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-bug30243.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-constraint-desc.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-doc-progmem.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-dwarf.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-libiberty-Makefile.in.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-newdevices.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-os_main-os_task.diff
   cerror "GCC patching failed"
#
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-zero_extend.diff
   cerror "GCC patching failed"
#  
   echo "($0) patching GCC source"
   patch -p0 < $archive/gcc-patch-zz-atmega256x.diff
   cerror "GCC patching failed"
#


   mkdir -p ../../build/${gcccore}
   cd ../../build/${gcccore}

   echo "($0) configuring GCC source"
   ../../source/${gccbase}/configure -v --target=${target} --disable-nls \
      --prefix=$prefix --with-gnu-ld --with-gnu-as --enable-languages="c,c++" --quiet --disable-libssp --with-dwarf2
   cerror "GCC configuration failed"

   echo "($0) building GCC"
#   make all install clean LANGUAGES="c obj-c++"
   make  all install clean LANGUAGES="c c++"
   cerror "GCC build failed"

   cd $prefix/source

   echo "($0) installing libc"
   tar xvjf $archive/${avrlibctar}
   cerror "libc source installation failed"

   cd $avrlibcbase
#   sh reconf
#   sh doconf
#   ./bootstrap
#   cerror "libc source setup failed"

   mkdir -p ../../build/${avrlibcbase}
   cd ../../build/${avrlibcbase}

   echo "($0) configuring libc source"
   CC=$prefix/bin/avr-gcc ../../source/${avrlibcbase}/configure -v \
      --build=`../../source/${avrlibcbase}/config.guess` --target=${target} --host=avr --prefix=$prefix --quiet
   cerror "libc configuration failed"

   echo "($0) building libc"
   make  all install clean
   cerror "libc build failed"

   # strip all the binaries
   find $prefix -type d -name bin -exec find \{\} -type f \; | xargs strip > /dev/null 2>&1

   cecho "\n"
   cecho "${cyan}installation of ${target} GNU tools complete\n"
   cecho "${cyan}add ${GREEN}$prefix/bin${cyan} to your path to use the ${target} GNU tools\n"
   cecho "${cyan}you might want to run the following to save disk space:\n"
   cecho "\n"
   cecho "${green}rm -rf $prefix/source $prefix/build\n"
}

# color definitions
RED='\e[1;31m'
green='\e[0;32m'
GREEN='\e[1;32m'
cyan='\e[0;36m'
yellow='\e[0;33m'
NC='\e[0m' # no color

function cecho()
{
   echo -ne "($green$0$NC) $1$NC"
}

function cerror()
{
   if [ $? -ne 0 ];
   then
      cecho "$RED$1$NC\n"
      exit
   fi
}

function ask()
{
   cecho "$@ [y/n] "
   read ans

   case "$ans" in
      y*|Y*) return 0 ;;
      *) return 1 ;;
   esac
}

# source command line overrides

until [ -z "$1" ];
do
   eval "$1"
   shift
done

cecho "${cyan}about to build and install ${target} GNU development tools using\n"
cecho "${cyan}the following settings (override via the command line):\n"
cecho "\n"
cecho "   ${yellow}archive=${archive} $cyan(location of source tarballs)\n"
cecho "   ${yellow}prefix=${prefix} $cyan(installation prefix/directory)\n"
cecho "   ${yellow}buildlog=${buildlog} $cyan(build log)\n"
cecho "\n"
ask "${cyan}proceed?";

if [ "$?" -eq 1 ]
then
   exit
fi

# check on target directory

if [ -d $prefix ];
then
   cecho "\n"
   ask "${RED}$prefix already exists, continue?";

   if [ "$?" -eq 1 ]
   then
      exit
   fi
fi

mkdir -p $prefix 2> /dev/null

if [ ! -w $prefix ];
then
   cecho "\n"
   cecho "${RED}failed! to create install directory $prefix\n";
   exit
fi

# check for required files

missingfiles=;

for file in $sourcefiles;
do
   if [ ! -f $archive/$file ];
   then
      missingfiles="${missingfiles} $file";
   fi
done

if [ -n "$missingfiles" ];
then
   cecho "\n"
   cecho "${RED}error! required source file(s):\n";
   cecho "\n"

   for file in $missingfiles;
   do
      cecho "   ${yellow}$file\n"
   done

   cecho "\n"
   cecho "${cyan}were missing - download them to $archive first\n";
   exit
fi

buildandinstall 2>&1 | tee $buildlog

exit

