
#
# Get GDB Insight
#
#wget. ftp://sources.redhat.com/pub/gdb/releases/insight-6.1.tar.bz2

#
# Get Binutils
#
wget -c ftp://ftp.dkuug.dk/pub/gnu/ftp/gnu/binutils/binutils-2.18.tar.bz2

#
# Get avr-libc
#
#Updated to avr-libc-1.6.1
#
wget -c http://download.savannah.gnu.org/releases/avr-libc/avr-libc-user-manual-1.6.1.pdf.bz2
wget -c http://download.savannah.gnu.org/releases/avr-libc/avr-libc-1.6.1.tar.bz2

#
# Get GCC
#

wget -c ftp://ftp.dkuug.dk/pub/gnu/ftp/gnu/gcc/gcc-4.2.2/gcc-4.2.2.tar.bz2
#
# Done
#

