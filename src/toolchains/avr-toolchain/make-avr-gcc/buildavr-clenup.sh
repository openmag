#!/bin/bash

# AVR GNU development tools install script
# $Id: buildavr.sh,v 1.21 2004/11/09 02:25:48 rmoffitt Exp $
# Modified by AJ Erasmus    2005/03/22
# Use the same source and patches as for WinAVR 20050214
# Updated script to compile the C and C++ compilers, as well
# as allow the output of dwarf-2 debug information 2005/03/25
# Updated script to add new devices 2005/03/26


# Cleanup the sources , and binaries og the previous build

# GNU tools will be installed under this directory
prefix=/usr/local/avr

rm -rf $prefix/source $prefix/build

exit

