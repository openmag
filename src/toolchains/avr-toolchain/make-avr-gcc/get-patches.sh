#
#Cleanup
#
rm -f *.diff
#
# Get binutils patches
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-binutils/files/patch-newdevices
#
# Rename to our patchname
#
mv patch-newdevices binutils-patch-newdevices.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-binutils/files/patch-aa
#
# Rename to our patchname
#
mv patch-aa binutils-patch-aa.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-binutils/files/patch-bug5215
#
# Rename to our patchname
#
mv patch-bug5215 binutils-patch-bug5215.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-binutils/files/patch-coff-avr
#
# Rename to our patchname
#
mv patch-coff-avr binutils-patch-coff-avr.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-binutils/files/patch-newsections
#
# Rename to our patchname
#
mv patch-newsections binutils-patch-newsections.diff
#
# Get WinAVR avr-size patch , NOTE ... 2.18 is in the patch path
#
wget --user-agent=MyBrowser http://winavr.cvs.sourceforge.net/*checkout*/winavr/patches/binutils/2.18/30-binutils-2.18-avr-size.patch
#
# Rename to our patchname
#
mv 30-binutils-2.18-avr-size.patch binutils-patch-avr-size.diff



#
# Get gcc patches
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-0b-constants
#
# Rename to our patchname
#
mv patch-0b-constants gcc-patch-0b-constants.diff
#
#
#wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-attribute_alias
#
# Rename to our patchname
#
#mv patch-attribute_alias gcc-patch-attribute_alias.diff
#
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-bug11259
#
# Rename to our patchname
#
mv patch-bug11259 gcc-patch-bug11259.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-bug25672
#
# Rename to our patchname
#
mv patch-bug25672 gcc-patch-bug25672.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-bug30243
#
# Rename to our patchname
#
mv patch-bug30243 gcc-patch-bug30243.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-constraint-desc
#
# Rename to our patchname
#
mv patch-constraint-desc gcc-patch-constraint-desc.diff
#
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-doc-progmem
#
# Rename to our patchname
#
mv patch-doc-progmem gcc-patch-doc-progmem.diff
#
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-dwarf
#
# Rename to our patchname
#
mv patch-dwarf gcc-patch-dwarf.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-libiberty-Makefile.in
#
# Rename to our patchname
#
mv patch-libiberty-Makefile.in gcc-patch-libiberty-Makefile.in.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-newdevices
#
# Rename to our patchname
#
mv patch-newdevices gcc-patch-newdevices.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-os_main-os_task
#
# Rename to our patchname
#
mv patch-os_main-os_task gcc-patch-os_main-os_task.diff
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-zero_extend
#
# Rename to our patchname
#
mv patch-zero_extend gcc-patch-zero_extend.diff
#
#
wget --user-agent=MyBrowser http://www.freebsd.org/cgi/cvsweb.cgi/~checkout~/ports/devel/avr-gcc/files/patch-zz-atmega256x
#
# Rename to our patchname
#
mv patch-zz-atmega256x gcc-patch-zz-atmega256x.diff
#

