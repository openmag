#!/bin/bash

# Note: this builds an avr toolchain
# Author: Sean D'Epagnier

# if $PREFIX/bin is added to PATH this script makes a complete
# toolchain for arm

export PREFIX=/usr/local
export PATH=$PATH:$PREFIX/bin

# if the host is down for whatever reason.. fetch the files manually

export SERVER=ftp://ftp.gnu.org/

wget $SERVER/gnu/binutils/binutils-2.18.tar.bz2
tar xjf binutils-2.18.tar.bz2 
cd binutils-2.18
./configure --prefix=$PREFIX --target=arm-elf --enable-interwork 
make
make install
cd ..
#rm -fr binutils-2.18

wget $SERVER/gnu/gcc/gcc-4.2.2/gcc-4.2.2.tar.bz2
tar xjf gcc-4.2.2.tar.bz2
cd gcc-4.2.2
./configure --prefix=$PREFIX --target=arm-elf --enable-interwork --enable-languages=c --with-newlib --disable-libssp --without-headers
make
make install
cd ..

wget ftp://sources.redhat.com/pub/newlib/newlib-1.16.0.tar.gz
tar xzf newlib-1.16.0.tar.gz
cd newlib-1.16.0
./configure --prefix=$PREFIX --target=arm-elf --disable-newlib-supplied-syscalls --enable-interwork
make
make install
cd ..
#rm -rf newlib-1.16.0

cd gcc-4.2.2
./configure --prefix=$PREFIX --target=arm-elf --enable-languages=c --with-newlib --disable-libssp --enable-interwork
make
make install
cd ..
#rm -rf gcc-4.2.2

wget $SERVER/gnu/gdb/gdb-6.7.tar.bz2
tar xjvf gdb-6.7.tar.bz2
cd gdb-6.7
./configure --prefix=$PREFIX --target=arm-elf --with-newlib --enable-interwork
make
make install
cd ..
#rm -rf gdb-6.7

svn checkout svn://svn.berlios.de/openocd/trunk
mv trunk openocd
cd openocd
./bootstrap
./configure --enable-parport --enable-parport_ppdev
make
make install
cd ..
#r m-rf openocd
