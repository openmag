#!/bin/bash

# this is a simple script to read measurements from the device, and create a sxv file

MEASUREMENTS=$(./dataclient -c "get survey/measurements")
if (( $? != 0 )); then
    echo "Failed to read measurements from the device, cannot generate sxv file";
    exit 1;
fi

echo Please enter the name of the survey
#NAME=test
read NAME

FILENAME=$NAME.svx

echo *begin $NAME > $FILENAME
echo *date `date +%Y.%m.%d` >> $FILENAME
echo *instrument compass DUSI >> $FILENAME
echo *instrument clino DUSI >> $FILENAME
echo >> $FILENAME

FIRST=1
while read line
do
    if (( !FIRST)); then 
        IFS=" " read FROM TO DISTANCE AZIMUTH INCLINE <<< $line;
        echo $FROM $TO $DISTANCE `echo $AZIMUTH | sed s,/.*,,g` `echo $INCLINE | sed s,/.*,,g` >> $FILENAME;
    fi
    FIRST=0;
done <<< "$MEASUREMENTS"

echo >> $FILENAME
echo *end $NAME >> $FILENAME

echo "Wrote $FILENAME"
