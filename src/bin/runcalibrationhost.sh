#!/bin/bash

# a simple script to set up a device to stream raw sensor data
# to feed into calibrationhost or menuhost

send_cmd () {
    ./dataclient -c "$1" 2>>log
    if [ $? -gt 0 ]; then
        echo "failed to access device!"
        exit 1
    fi
}

echo "setting up the device to stream raw data"
send_cmd "set sensors/accel/outputrate 1000"
send_cmd "set sensors/accel/outputtype raw"
send_cmd "set sensors/mag/outputrate 1000"
send_cmd "set sensors/mag/outputtype raw"
send_cmd "set sensors/temp/autooutput true"
send_cmd "set sensors/temp/outputtype raw"
