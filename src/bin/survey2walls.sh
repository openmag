#!/bin/bash

# this is a simple script to read measurements from the device, and create a walls srv file

MEASUREMENTS=$(./dataclient -c "get survey/measurements")
if (( $? != 0 )); then
    echo "Failed to read measurements from the device, cannot generate srv file";
    exit 1;
fi

echo \;Inst: DUSI
echo \#date `date +%Y-%m-%d`

FIRST=1
while read line
do
    if (( !FIRST)); then 
        IFS=" " read FROM TO DISTANCE AZIMUTH INCLINE LEFT RIGHT UP DOWN<<< $line;
        echo -en "$FROM\t$TO\t$DISTANCE\t$AZIMUTH\t$INCLINE";
        if [ "$LEFT" = "" ]; then
            echo
        else
            echo " " *$LEFT,$RIGHT,$UP,$DOWN*;
        fi
    fi
    FIRST=0;
done <<< "$MEASUREMENTS"
