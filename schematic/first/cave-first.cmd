# Pin name action command file

# Start of element C102
ChangePinName(C102, 2, 2)
ChangePinName(C102, 1, 1)

# Start of element R101
ChangePinName(R101, 1, 1)
ChangePinName(R101, 2, 2)

# Start of element U103
ChangePinName(U103, 10, OUTx-)
ChangePinName(U103, 9, GND1)
ChangePinName(U103, 8, S/R-)
ChangePinName(U103, 7, OUTy-)
ChangePinName(U103, 6, S/R+)
ChangePinName(U103, 5, Vcc)
ChangePinName(U103, 4, OUTx+)
ChangePinName(U103, 3, GND2)
ChangePinName(U103, 2, OUTy+)
ChangePinName(U103, 1, GND)

# Start of element C101
ChangePinName(C101, 2, 2)
ChangePinName(C101, 1, 1)

# Start of element U102
ChangePinName(U102, 8, OUT-)
ChangePinName(U102, 7, S/R-)
ChangePinName(U102, 6, S/R+)
ChangePinName(U102, 5, GND2)
ChangePinName(U102, 4, GND)
ChangePinName(U102, 3, Vcc)
ChangePinName(U102, 2, OUT+)
ChangePinName(U102, 1, GND1)

# Start of element U101
ChangePinName(U101, 16, DIN)
ChangePinName(U101, 15, DOUT/RDY)
ChangePinName(U101, 14, DVdd)
ChangePinName(U101, 8, AIN2(-))
ChangePinName(U101, 7, AIN2(+))
ChangePinName(U101, 6, AIN1(-))
ChangePinName(U101, 13, AVdd)
ChangePinName(U101, 12, GND)
ChangePinName(U101, 11, PSW)
ChangePinName(U101, 10, REFIN(-))
ChangePinName(U101, 9, REFIN(+))
ChangePinName(U101, 5, AIN1(+))
ChangePinName(U101, 4, AIN3(-)/P2)
ChangePinName(U101, 3, AIN3(+)/P1)
ChangePinName(U101, 2, CS)
ChangePinName(U101, 1, SCLK)

# Start of element CONN106
ChangePinName(CONN106, 5, 5)
ChangePinName(CONN106, 3, 3)
ChangePinName(CONN106, 1, 1)
ChangePinName(CONN106, 4, 4)
ChangePinName(CONN106, 2, 2)

# Start of element CONN105
ChangePinName(CONN105, 1, 1)
ChangePinName(CONN105, 2, 2)

# Start of element CONN103
ChangePinName(CONN103, 5, 5)
ChangePinName(CONN103, 3, 3)
ChangePinName(CONN103, 1, 1)
ChangePinName(CONN103, 4, 4)
ChangePinName(CONN103, 2, 2)

# Start of element C106
ChangePinName(C106, 2, 2)
ChangePinName(C106, 1, 1)

# Start of element CONN102
ChangePinName(CONN102, 11, 11)
ChangePinName(CONN102, 9, 9)
ChangePinName(CONN102, 12, 12)
ChangePinName(CONN102, 10, 10)
ChangePinName(CONN102, 7, 7)
ChangePinName(CONN102, 5, 5)
ChangePinName(CONN102, 3, 3)
ChangePinName(CONN102, 1, 1)
ChangePinName(CONN102, 8, 8)
ChangePinName(CONN102, 6, 6)
ChangePinName(CONN102, 4, 4)
ChangePinName(CONN102, 2, 2)

# Start of element T1
ChangePinName(T1, 1, terminal)

# Start of element R104
ChangePinName(R104, 1, 1)
ChangePinName(R104, 2, 2)

# Start of element CONN101
ChangePinName(CONN101, 3, 3)
ChangePinName(CONN101, 1, 1)
ChangePinName(CONN101, 2, 2)

# Start of element C105
ChangePinName(C105, 2, 2)
ChangePinName(C105, 1, 1)

# Start of element U106
ChangePinName(U106, 8, D1)
ChangePinName(U106, 7, D1)
ChangePinName(U106, 6, D2)
ChangePinName(U106, 5, D2)
ChangePinName(U106, 4, G2)
ChangePinName(U106, 3, S2)
ChangePinName(U106, 2, G1)
ChangePinName(U106, 1, S1)

# Start of element C104
ChangePinName(C104, 2, 2)
ChangePinName(C104, 1, 1)

# Start of element R103
ChangePinName(R103, 1, 1)
ChangePinName(R103, 2, 2)

# Start of element U105
ChangePinName(U105, 18, NC)
ChangePinName(U105, 17, ATSTIO)
ChangePinName(U105, 16, AVSS)
ChangePinName(U105, 15, AVSS)
ChangePinName(U105, 14, AVDD)
ChangePinName(U105, 13, MOSI)
ChangePinName(U105, 12, MISO)
ChangePinName(U105, 11, SCK)
ChangePinName(U105, 10, NC)
ChangePinName(U105, 9, NC)
ChangePinName(U105, 8, CSB)
ChangePinName(U105, 7, DVIO)
ChangePinName(U105, 6, DVDD)
ChangePinName(U105, 5, DVSS)
ChangePinName(U105, 4, CLK)
ChangePinName(U105, 3, INT)
ChangePinName(U105, 2, XRESET)
ChangePinName(U105, 1, NC)

# Start of element C103
ChangePinName(C103, 2, 2)
ChangePinName(C103, 1, 1)

# Start of element U104
ChangePinName(U104, 22, (PCINT19/TMS) PC3)
ChangePinName(U104, 21, (PCINT18/TCK) PC2)
ChangePinName(U104, 43, PB3(AIN1/OC0A/PCINT11))
ChangePinName(U104, 44, PB4 (SS/OC0B/PCINT12))
ChangePinName(U104, 35, PA2 (ADC2/PCINT2))
ChangePinName(U104, 36, PA1 (ADC1/PCINT1))
ChangePinName(U104, 37, PA0 (ADC0/PCINT0))
ChangePinName(U104, 38, VCC)
ChangePinName(U104, 39, GND)
ChangePinName(U104, 40, PB0 (XCK0/T0/PCINT8))
ChangePinName(U104, 41, PB1 (T1/CLKO/PCINT9))
ChangePinName(U104, 42, PB2 (AIN0/INT2/PCINT10))
ChangePinName(U104, 34, PA3 (ADC3/PCINT3))
ChangePinName(U104, 33, PA4 (ADC4/PCINT4))
ChangePinName(U104, 10, (PCINT25/TXD0) PD1)
ChangePinName(U104, 11, (PCINT26/INT0) PD2)
ChangePinName(U104, 12, (PCINT27/INT1) PD3)
ChangePinName(U104, 13, (PCINT28/OC1B) PD4)
ChangePinName(U104, 14, (PCINT29/OC1A) PD5)
ChangePinName(U104, 15, (PCINT30/OC2B/ICP) PD6)
ChangePinName(U104, 16, (PCINT32/OC2A) PD7)
ChangePinName(U104, 17, VCC)
ChangePinName(U104, 25, PC6 (TOSC1/PCINT22))
ChangePinName(U104, 26, PC7 (TOSC2/PCINT23))
ChangePinName(U104, 27, AVCC)
ChangePinName(U104, 28, GND)
ChangePinName(U104, 29, AREF)
ChangePinName(U104, 30, PA7 (ADC7/PCINT7))
ChangePinName(U104, 31, PA6 (ADC6/PCINT6))
ChangePinName(U104, 32, PA5 (ADC5/PCINT5))
ChangePinName(U104, 4, RESET)
ChangePinName(U104, 5, VCC)
ChangePinName(U104, 6, GND)
ChangePinName(U104, 7, XTAL2)
ChangePinName(U104, 8, XTAL1)
ChangePinName(U104, 9, (PCINT24/RXD0) PD0)
ChangePinName(U104, 20, (PCINT17/SDA) PC1)
ChangePinName(U104, 19, (SCL/PCINT16) PC0)
ChangePinName(U104, 18, GND)
ChangePinName(U104, 24, PC5 (TDI/PCINT21))
ChangePinName(U104, 23, PC4 (TDO/PCINT20))
ChangePinName(U104, 3, (PCINT15/SCK) PB7)
ChangePinName(U104, 2, (PCINT14/MISO) PB6)
ChangePinName(U104, 1, (PCINT13/MOSI) PB5)

# Start of element R102
ChangePinName(R102, 1, 1)
ChangePinName(R102, 2, 2)
